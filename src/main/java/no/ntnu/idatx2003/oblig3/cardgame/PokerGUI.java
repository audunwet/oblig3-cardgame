package no.ntnu.idatx2003.oblig3.cardgame;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.List;
import java.util.stream.Collectors;


public class PokerGUI extends Application {

  private DeckOfCards deck;
  private List<PlayingCard> hand;
  private Text handDisplay;
  private Label sumLabel;
  private Label heartsLabel;
  private Label flushLabel;
  private Label queenOfSpadesLabel;

  @Override
  public void start(Stage stage) throws Exception {

    deck = new DeckOfCards(); // Initialize the deck
    handDisplay = new Text("Hand Display");
    sumLabel = new Label("Sum of the faces:");
    heartsLabel = new Label("Cards of hearts:");
    flushLabel = new Label("Flush:");
    queenOfSpadesLabel = new Label("Queen of spades:");

    Button dealHandButton = new Button("Deal hand");
    Button checkHandButton = new Button("Check hand");
    // Set up event handlers
    dealHandButton.setOnAction(event -> dealHand());
    checkHandButton.setOnAction(event -> checkHand());


    VBox handInfo = new VBox(10, handDisplay, sumLabel, heartsLabel, flushLabel, queenOfSpadesLabel);

    handInfo.setMaxSize(200,150);
    handInfo.setMinSize(200,150);

    String cssLayout = "-fx-border-color: black;\n" +
        "-fx-border-insets: 5;\n" +
        "-fx-border-width: 3;\n";
    handInfo.setStyle(cssLayout);

    VBox buttonHolder = new VBox(10, dealHandButton, checkHandButton);
    buttonHolder.setPadding(new javafx.geometry.Insets(40,0,0,0));

    HBox ItemHolder = new HBox(20, buttonHolder, handInfo);
    BorderPane layout = new BorderPane();
    layout.setPadding(new javafx.geometry.Insets(10, 10, 10, 10));

    layout.setCenter(ItemHolder);




    Scene scene = new Scene(layout, 350, 200);
    stage.setTitle("Five Hand Poker");
    stage.setScene(scene);
    stage.show();
  }

  private void dealHand() {
    //check if five available cards left in deck
    if (deck.getDeckSize()<5){
      deck = new DeckOfCards();
    } else {
      hand = deck.dealHand(5); // Let's say we deal 5 cards
      displayHand();
    }
  }


  /**
   * This method checks the hand for the sum of the faces,
   * the cards of hearts,
   * if the hand is a flush and if the hand contains the queen of spades.
   */
  private void checkHand() {
    if (hand == null || hand.isEmpty()) {
      return; // No hand to check
    }

    int sum = hand.stream().mapToInt(PlayingCard::getFace).sum();
    sumLabel.setText("Sum of the faces: " + sum);

    String hearts = hand.stream()
        .filter(card -> card.getSuit() == 'H')
        .map(PlayingCard::getAsString)
        .collect(Collectors.joining(" "));

    if (hearts.isEmpty()) {
      heartsLabel.setText("Cards of hearts: No hearts");
    } else {
      heartsLabel.setText("Cards of hearts: " + hearts);
    }

    boolean isFlush = hand.stream().map(PlayingCard::getSuit).distinct().count() == 1;
    flushLabel.setText("Flush: " + (isFlush ? "Yes" : "No"));

    boolean hasQueenOfSpades = hand.stream().anyMatch(card -> card.getFace() == 12 && card.getSuit() == 'S');
    queenOfSpadesLabel.setText("Queen of spades: " + (hasQueenOfSpades ? "Yes" : "No"));
  }

  private void displayHand() {
    StringBuilder handBuilder = new StringBuilder();
    for (PlayingCard card : hand) {
      handBuilder.append(card.getAsString()).append(" ");
    }
    handDisplay.setText(handBuilder.toString());
  }

  public static void main(String[] args) {
    launch(args);
  }

}