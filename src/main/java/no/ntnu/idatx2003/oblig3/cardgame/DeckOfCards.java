package no.ntnu.idatx2003.oblig3.cardgame;

import java.util.*;

// Represents a deck of playing cards
public class DeckOfCards {
  private List<PlayingCard> deck;
  private final char[] suits = {'S', 'H', 'D', 'C'};
  private final int NUMBER_OF_CARDS = 52;
  private final int NUMBER_OF_SUITS = 4;
  private final int CARDS_PER_SUIT = 13;

  // Creates a new deck of cards
  public DeckOfCards() {
    deck = new ArrayList<>(NUMBER_OF_CARDS);
    for (char suit : suits) {
      for (int face = 1; face <= CARDS_PER_SUIT; face++) {
        deck.add(new PlayingCard(suit, face));
      }
    }
  }

  public List<PlayingCard> dealHand(int n) {
    if (n < 1 || n > NUMBER_OF_CARDS) {
      throw new IllegalArgumentException("Number of cards to draw must be between 1 and " + NUMBER_OF_CARDS);
    }
    Collections.shuffle(deck, new Random());
    List<PlayingCard> hand = new ArrayList<>(n);
    for (int i = 0; i < n; i++) {
      hand.add(deck.remove(deck.size() - 1));
    }
    return hand;
  }

  public int getDeckSize() {
    return deck.size();
  }
}
